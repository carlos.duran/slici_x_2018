#!/bin/bash
#set -x #coloca los comandos que se van ejecutando, se mantiene comentado
primerNombre="Carlos"
primerApellido="Duran"
fecha=$(date +%Y%m%d_%H%M%S)
#fecha2=`date +%Y%m%d_%H%M%S`
#usuario=$USER
usuario="soporte"

echo "Hola ${primerNombre} ${primerApellido}!"
echo "La fecha es: $fecha"
#echo $fecha2

# Lectura de variables, se pasan todos los caracteres a minusculas

echo "Cuál es su departamento? ->"
read depto
depto=$(echo $depto | tr '[A-Z]' '[a-z]')

echo "Cuál es su nombre>? ->"
read nombre
nombre=$(echo $depto | tr '[A-Z]' '[a-z]')

#los parentesis cuadrados ejecutan el comando test, siempre un comando
#if tiene que tener los parentesis cuadrados para comparar

if [ $usuario == $depto ]
then
  echo "Bienvenido ${primerNombre} ${primerApellido}"
  echo "Del departamento de ${depto}"
else
  echo "Hola ${nombre2}..."
fi #cierre del if
